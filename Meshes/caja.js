{

    "metadata" :
    {
        "formatVersion" : 3.1,
        "sourceFile"    : "caja.obj",
        "generatedBy"   : "OBJConverter",
        "vertices"      : 8,
        "faces"         : 6,
        "normals"       : 6,
        "colors"        : 0,
        "uvs"           : 4,
        "materials"     : 1
    },

    "scale" : 1.000000,

    "materials": [	{
	"DbgColor" : 15658734,
	"DbgIndex" : 0,
	"DbgName" : "Material__35",
	"colorAmbient" : [0.588, 0.588, 0.588],
	"colorDiffuse" : [0.588, 0.588, 0.588],
	"colorSpecular" : [0.0, 0.0, 0.0],
	"illumination" : 2,
	"mapAmbient" : "crate_1.jpg",
	"mapDiffuse" : "crate_1.jpg",
	"opticalDensity" : 1.5,
	"specularCoef" : 10.0,
	"transparency" : 0.0,
	"transparent" : true
	}],

    "vertices": [-1.001524,1.997200,-1.001569,-1.001524,-0.002800,-1.001569,-1.001524,-0.002800,0.998431,-1.001524,1.997200,0.998431,0.998476,1.997200,-1.001569,0.998476,-0.002800,-1.001569,0.998476,1.997200,0.998431,0.998476,-0.002800,0.998431],

    "morphTargets": [],

    "morphColors": [],

    "normals": [-1,0,-0,0,0,-1,1,0,-0,0,0,1,0,-1,-0,0,1,-0],

    "colors": [],

    "uvs": [[1,0,1,1,0,1,0,0]],

    "faces": [43,0,1,2,3,0,0,1,2,3,0,0,0,0,43,4,5,1,0,0,0,1,2,3,1,1,1,1,43,6,7,5,4,0,0,1,2,3,2,2,2,2,43,3,2,7,6,0,0,1,2,3,3,3,3,3,43,1,5,7,2,0,0,1,2,3,4,4,4,4,43,4,0,3,6,0,0,1,2,3,5,5,5,5]

}
