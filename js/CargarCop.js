function cargarCop(){
	//Porsche(MaxSpeed, Aceleracion, Manejabilidad, name)
	
	var manejabilidad = 0.06;
	var movSpeed = 3;									
	var frenos = 0.99;
	var nitro = 1.02;

	var C1 = 10;
	var C2 = 25;
	var C3 = 45;
	var C4 = 65;
	var C5 = 80;

	VariablesCarro.push(manejabilidad);
	VariablesCarro.push(movSpeed);
	VariablesCarro.push(frenos);
	VariablesCarro.push(nitro);

	VariablesCarro.push(C1);
	VariablesCarro.push(C2);
	VariablesCarro.push(C3);
	VariablesCarro.push(C4);
	VariablesCarro.push(C5);
	//ELEMENTOS CARRO
	//------------------carroceria----------------------
	var carroceriaMeshLoader = new THREE.JSONLoader();
	var carroceriaMeshFuntion = function( geometry )
	{	
		var carroceriaMaterial = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./Meshes/cop/0000.jpg')});
	    var carroceriaMesh = new THREE.Mesh( geometry, carroceriaMaterial );
	    carroceriaMesh.position.set( 0, 5, 0 );
	    carroceriaMesh.scale.set( 30, 30, 30 );
	    carroceriaMesh.castShadow = true;
	    carroceriaMesh.receiveShadow = false;
	    carroceriaMesh.overdraw = true;
	    carroceriaMesh.rotation.y = (Math.PI);
	    ArregloCarro.Carroceria = carroceriaMesh; 
	    //scene.agregar(ArregloCarro["Carroceria"]);
	    //porsche.agregar( carroceriaMesh );
	    
	    cargador.objReady();
	};
	carroceriaMeshLoader.load( "./Meshes/cop/Carroceria.js", carroceriaMeshFuntion );
	//------------------vidrios----------------------
	var glassesMeshLoader = new THREE.JSONLoader();
	var glassesMeshFuntion = function( geometry )
	{	
		var glassesMaterial = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./Meshes/cop/0000.jpg'), transparent: true});
	    glassesMaterial.opacity = 0.2;
	    var glassesMesh = new THREE.Mesh( geometry, glassesMaterial );
	    glassesMesh.position.set( 0, 5, 0 );
	    glassesMesh.scale.set( 30, 30, 30 );
	    glassesMesh.castShadow = true;
	    glassesMesh.receiveShadow = true;
	    glassesMesh.overdraw = true;   
	    glassesMesh.rotation.y = (Math.PI);
	    ArregloCarro.Glasses = glassesMesh; 
	    //scene.agregar(ArregloCarro["Glasses"]);
	    //porsche.agregar( glassesMesh );
	    cargador.objReady();
	};
	glassesMeshLoader.load( "./Meshes/cop/Glasses.js",  glassesMeshFuntion );
		//------------------rear_wheels----------------------
	var rear_wheelsMeshLoader = new THREE.JSONLoader();
	var rear_wheelsMeshFuntion = function( geometry )
	{	
		var rear_wheelsMaterial = new THREE.MeshLambertMaterial({map: new THREE.ImageUtils.loadTexture('./Meshes/cop/wheels.jpg')});
	    //rear_wheelsMaterial.opacity = 0.2;
	    var rear_wheelsMesh = new THREE.Mesh( geometry, rear_wheelsMaterial );
	    rear_wheelsMesh.position.set( 0, 5, 0 );
	    rear_wheelsMesh.scale.set( 30, 30, 30 );
	    rear_wheelsMesh.castShadow = true;
	    rear_wheelsMesh.receiveShadow = true;
	    rear_wheelsMesh.overdraw = true;	   
	    ArregloCarro.Rear_wheels = rear_wheelsMesh; 
	    //scene.agregar(ArregloCarro["Rear_wheels"]);
	    //porsche.agregar( rear_wheelsMesh );
	    cargador.objReady();
	};
	rear_wheelsMeshLoader.load( "./Meshes/cop/RearWheels.js", rear_wheelsMeshFuntion );
	//------------------R_wheel----------------------
	var R_wheelMeshLoader = new THREE.JSONLoader();
	var R_wheelMeshFuntion = function( geometry )
	{	
		var R_wheelMaterial = new THREE.MeshLambertMaterial({map: new THREE.ImageUtils.loadTexture('./Meshes/cop/wheels.jpg')});
	    //R_wheelMaterial.opacity = 0.2;
	    var R_wheelMesh = new THREE.Mesh( geometry, R_wheelMaterial );
	    R_wheelMesh.position.set( 22, 5, -88 );
	    R_wheelMesh.scale.set( 30, 30, 30 );
	    R_wheelMesh.castShadow = true;
	    R_wheelMesh.receiveShadow = true;
	    R_wheelMesh.overdraw = true;	   
	    ArregloCarro.R_wheel = R_wheelMesh; 
	    //scene.agregar(ArregloCarro["R_wheel"]);
		//porsche.agregar( R_wheelMesh );
	    cargador.objReady();
	};
	R_wheelMeshLoader.load( "./Meshes/cop/R_Wheel.js", R_wheelMeshFuntion );
	//------------------L_wheel----------------------
	var L_wheelMeshLoader = new THREE.JSONLoader();
	var L_wheelMeshFuntion = function( geometry )
	{	
		var L_wheelMaterial = new THREE.MeshLambertMaterial({map: new THREE.ImageUtils.loadTexture('./Meshes/cop/wheels.jpg')});
	    //L_wheelMaterial.opacity = 0.2;
	    var L_wheelMesh = new THREE.Mesh( geometry, L_wheelMaterial );
	    L_wheelMesh.position.set( -22, 5, -88 );
	    L_wheelMesh.scale.set( 30, 30, 30 );
	    L_wheelMesh.castShadow = true;
	    L_wheelMesh.receiveShadow = true;
	    L_wheelMesh.overdraw = true;	 
	    ArregloCarro.L_wheel = L_wheelMesh; 
	    //scene.agregar(ArregloCarro["L_wheel"]);
	    //porsche.agregar( L_wheelMesh );
		cargador.objReady();
	};
	L_wheelMeshLoader.load( "./Meshes/cop/L_Wheel.js", L_wheelMeshFuntion )

	//boundingBox
	var geometry = new THREE.CubeGeometry(55,50,155,5,5,5);
	var material = new THREE.MeshLambertMaterial({color:0x000000,wireframe:true});
	boundingBox = new THREE.Mesh(geometry,material);
	boundingBox.position.set(0,20,-40);
	boundingBox.name = "cajaDeColision";
	boundingBox.visible = false;
	ArregloCarro.BB = boundingBox;
}