var Porsche = function(MaxSpeed, Aceleracion, Manejabilidad){
	this.set(MaxSpeed, Aceleracion, Manejabilidad);
	mesh = new THREE.Object3D();
	mesh.name = "Porshe_911GT";
}

	Porsche.prototype = {
		set: function(MaxSpeed, Aceleracion, Manejabilidad){
			this.MaxSpeed = MaxSpeed || 0;
            this.Aceleracion = Aceleracion || 0;
            this.Manejabilidad = Manejabilidad || 0;
            return this;
		}
		setMaxSpeed: function(MaxSpeed){
			this.MaxSpeed = MaxSpeed;
            return this;
		}
		setAceleracion: function(Aceleracion){
			this.Aceleracion = Aceleracion;
            return this;
		}
		setManejabilidad: function(Manejabilidad){
			this.Manejabilidad = Manejabilidad;
            return this;
		}
		agregar: function(obj){
			mesh.add(obj);
			return mesh;
		}

	};