function cargarEscenario(){
	//------------------stop----------------------
		var stopMeshLoader1 = new THREE.JSONLoader();
		var stopMeshFunction1 = function( geometry )
		{	
			var stopMaterial1 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/stop.jpg')});
		    var stopMesh1 = new THREE.Mesh( geometry, stopMaterial1 );
		    stopMesh1.position.set( 1427, 0, -930 );
		    stopMesh1.rotation.y = -Math.PI/2;
		    stopMesh1.scale.set( 20, 20, 20 );
		    stopMesh1.castShadow = true;
		    stopMesh1.receiveShadow = false;
		    stopMesh1.overdraw = true;
			collidableMeshList.push(stopMesh1);
		    ArregloEscenario.Stop = stopMesh1; 
		    scene.add(ArregloEscenario["Stop"]);
		    cargador.objReady();
		};
		stopMeshLoader1.load( "./js/escenario/stop.js", stopMeshFunction1 );

		var stopMeshLoader2 = new THREE.JSONLoader();
		var stopMeshFunction2 = function( geometry )
		{	
			var stopMaterial2 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/stop.jpg')});
		    var stopMesh2 = new THREE.Mesh( geometry, stopMaterial2 );
		    stopMesh2.position.set( -2100, 0, -1000 );
		    stopMesh2.scale.set( 20, 20, 20 );
		    stopMesh2.castShadow = true;
		    stopMesh2.receiveShadow = false;
		    stopMesh2.overdraw = true;
			collidableMeshList.push(stopMesh2);
		    ArregloEscenario.Stop = stopMesh2; 
		    scene.add(ArregloEscenario["Stop"]);
		    cargador.objReady();
		};
		stopMeshLoader2.load( "./js/escenario/stop.js", stopMeshFunction2 );	

	//------------------lightPole----------------------
		var lightPoleMeshLoader1 = new THREE.JSONLoader();
		var lightPoleMeshFunction1 = function( geometry )
		{	
			var lightPoleMaterial1 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/lightPole.jpg')});
		    var lightPoleMesh1 = new THREE.Mesh( geometry, lightPoleMaterial1 );
		    lightPoleMesh1.position.set( -1826.5, 5, -4 );
		    lightPoleMesh1.scale.set( 9, 9, 9 );
		    lightPoleMesh1.rotation.y = Math.PI;
		    lightPoleMesh1.castShadow = true;
		    lightPoleMesh1.receiveShadow = false;
		    lightPoleMesh1.overdraw = true;
			collidableMeshList.push(lightPoleMesh1);
		    ArregloEscenario.LightPole1 = lightPoleMesh1; 
		    scene.add(ArregloEscenario["LightPole1"]);
		    cargador.objReady();
		};
		lightPoleMeshLoader1.load( "./js/escenario/lightPole.js", lightPoleMeshFunction1 );

		
		var lightPoleMeshLoader2 = new THREE.JSONLoader();
		var lightPoleMeshFunction2 = function( geometry )
		{	
			var lightPoleMaterial2 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/lightPole.jpg')});
		    var lightPoleMesh2 = new THREE.Mesh( geometry, lightPoleMaterial2 );
		    lightPoleMesh2.position.set( 1391.5,5,-2090);
		    lightPoleMesh2.rotation.y = -Math.PI/2;
		    lightPoleMesh2.scale.set( 9, 9, 9 );
		    lightPoleMesh2.castShadow = true;
		    lightPoleMesh2.receiveShadow = false;
		    lightPoleMesh2.overdraw = true;
			collidableMeshList.push(lightPoleMesh2);
		    ArregloEscenario.LightPole2 = lightPoleMesh2; 
		    scene.add(ArregloEscenario["LightPole2"]);
		    cargador.objReady();
		};
		lightPoleMeshLoader2.load( "./js/escenario/lightPole.js", lightPoleMeshFunction2 );


		var lightPoleMeshLoader3 = new THREE.JSONLoader();
		var lightPoleMeshFunction3 = function( geometry )
		{	
			var lightPoleMaterial3 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/lightPole.jpg')});
		    var lightPoleMesh3 = new THREE.Mesh( geometry, lightPoleMaterial3 );
		    lightPoleMesh3.position.set( -352.5,5,151);
		    lightPoleMesh3.rotation.y = -Math.PI/4;
		    lightPoleMesh3.scale.set( 9, 9, 9 );
		    lightPoleMesh3.castShadow = true;
		    lightPoleMesh3.receiveShadow = false;
		    lightPoleMesh3.overdraw = true;
			collidableMeshList.push(lightPoleMesh3);
		    ArregloEscenario.LightPole2 = lightPoleMesh3; 
		    scene.add(ArregloEscenario["LightPole2"]);
		    cargador.objReady();
		};
		lightPoleMeshLoader3.load( "./js/escenario/lightPole.js", lightPoleMeshFunction3 );


		var lightPoleMeshLoader4 = new THREE.JSONLoader();
		var lightPoleMeshFunction4 = function( geometry )
		{	
			var lightPoleMaterial4 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/lightPole.jpg')});
		    var lightPoleMesh4 = new THREE.Mesh( geometry, lightPoleMaterial4 );
		    lightPoleMesh4.position.set( 853,5,-2090);
		    lightPoleMesh4.rotation.y = -Math.PI/2;
		    lightPoleMesh4.scale.set( 9, 9, 9 );
		    lightPoleMesh4.castShadow = true;
		    lightPoleMesh4.receiveShadow = false;
		    lightPoleMesh4.overdraw = true;
			collidableMeshList.push(lightPoleMesh4);
		    ArregloEscenario.LightPole2 = lightPoleMesh4; 
		    scene.add(ArregloEscenario["LightPole2"]);
		    cargador.objReady();
		};
		lightPoleMeshLoader4.load( "./js/escenario/lightPole.js", lightPoleMeshFunction4 );


		var lightPoleMeshLoader5 = new THREE.JSONLoader();
		var lightPoleMeshFunction5 = function( geometry )
		{	
			var lightPoleMaterial5 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/lightPole.jpg')});
		    var lightPoleMesh5 = new THREE.Mesh( geometry, lightPoleMaterial5 );
		    lightPoleMesh5.position.set( 307,5,-2090);
		    lightPoleMesh5.rotation.y = -Math.PI/2;
		    lightPoleMesh5.scale.set( 9, 9, 9 );
		    lightPoleMesh5.castShadow = true;
		    lightPoleMesh5.receiveShadow = false;
		    lightPoleMesh5.overdraw = true;
			collidableMeshList.push(lightPoleMesh5);
		    ArregloEscenario.LightPole2 = lightPoleMesh5; 
		    scene.add(ArregloEscenario["LightPole2"]);
		    cargador.objReady();
		};
		lightPoleMeshLoader5.load( "./js/escenario/lightPole.js", lightPoleMeshFunction5 );


		var lightPoleMeshLoader6 = new THREE.JSONLoader();
		var lightPoleMeshFunction6 = function( geometry )
		{	
			var lightPoleMaterial6 = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./js/escenario/lightPole.jpg')});
		    var lightPoleMesh6 = new THREE.Mesh( geometry, lightPoleMaterial6 );
		    lightPoleMesh6.position.set( 1339,5,1136);
		    lightPoleMesh6.rotation.y = -Math.PI/2;
		    lightPoleMesh6.scale.set( 9, 9, 9 );
		    lightPoleMesh6.castShadow = true;
		    lightPoleMesh6.receiveShadow = false;
		    lightPoleMesh6.overdraw = true;
			collidableMeshList.push(lightPoleMesh6);
		    ArregloEscenario.LightPole2 = lightPoleMesh6; 
		    scene.add(ArregloEscenario["LightPole2"]);
		    cargador.objReady();
		};
		lightPoleMeshLoader6.load( "./js/escenario/lightPole.js", lightPoleMeshFunction6 );
}