var contTerror = 0; 
var contAlegria = 0; 
var contRelax = 0; 
var contSuspenso = 0; 

var Sound = function ( sources, radius, volume ) {

	var audio = document.createElement( 'audio' );
	console.log(audio);
	for ( var i = 0; i < sources.length; i ++ ) {

		var source = document.createElement( 'source' );
		source.src = sources[ i ];

		audio.appendChild( source );

		console.log(audio.appendChild( source ).outerHTML);
	}

	this.position = new THREE.Vector3();

	this.play = function () {
		audio.loop = true;
		audio.play();
	}

	this.playOnce = function () {
		audio.loop = false;
		audio.play();
	}

	this.pause = function () {
		audio.pause();
	}

	this.Stop = function () {
		audio.pause();
		audio.currentTime = 0;
	}
	
	this.siguienteTerror = function(){

		if (contTerror < 1) {
			contTerror++;
		}; 	
		console.log(sources);		
	}
	
	this.siguienteRelax = function(){

		if (contTerror < 1) {
			contTerror++;
		}; 	
		console.log(sources);		
	}

	this.siguienteAmor = function(){

		if (contTerror < 1) {
			contTerror++;
		}; 	
		console.log(sources);		
	}

	this.siguienteAlegria = function(){

		if (contTerror < 1) {
			contTerror++;
		}; 	
		console.log(sources);		
	}


/*
	this.anterior = function(){

		if (contTerror > 0) {
			contTerror--;
		}else{
			contTerror = 0;
		} 	
	}*/

	this.retroseder = function () {
		audio.currentTime -= 1;
	}


	this.update = function ( target ) {

		var distance = this.position.distanceTo( target.position );

		if ( distance <= radius ) {

			audio.volume = volume * ( 1 - distance / radius );

		} else {

			audio.volume = 0;

		}

	}

}

