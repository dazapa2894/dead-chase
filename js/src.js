var scene,camera,renderer;
var ultiTiempo, clock, delta, time;
var labels = [];
var objetos = {};
var appW = window.innerWidth;
var appH = window.innerHeight;
var tacometro=document.getElementById("taco");
var aguja=document.getElementById("aguja");

var cronometro=document.getElementById("reloj");
var zombie=document.getElementById("zombie");
var score=document.getElementById("score");
var barra=document.getElementById("barra");
var Vnitro=document.getElementById("nitro");

var varCargar;

var ganar = 20;

var TECLA = { ARRIBA:false, 
			  ABAJO:false,
			  IZQUIERDA:false,
			  DERECHA:false, 
			  ESPACIO:false,
			  NITRO:false,

			  PRIMERA:false,
			  SEGUNDA:false,
			  TERCERA:false,
			  CUARTA:false,
			  QUINTA:false,
			  REVERSA:false,
			  NEUTRO:false
			};

var currentmin=5;	
var currentsec=59;	
var currentmil=99;
var keepgoin=false;		// keepgoin is false
function timer(){
 if(keepgoin){
   currentmil-= 1;		// add incretement
    if (currentmil==0){		// if miliseconds reach 10
     currentmil=59;		// Change miliseconds to zero
     currentsec-= 1;		// and add one to the seconds variable
    }
   if (currentsec==0){		// if seconds reach 60
    currentsec=59;		// Change seconds to zero
    currentmin-= 1;		// and add one to the minute variable
   }
  Strsec=""+currentsec;		// Convert to strings
  Strmin=""+currentmin;		// Convert to strings
  Strmil=""+currentmil;		// Convert to strings
   if (Strsec.length!=2){	// if seconds string is less than
    Strsec="0"+currentsec;	// 2 characters long, pad with leading
   }				// zeros
   if (Strmin.length!=2){	// Same deal here with minutes
    Strmin="0"+currentmin;
   }
  //document.display.seconds.value=Strsec		// displays times
  //document.display.minutes.value=Strmin;	// here
  //document.display.milsecs.value=Strmil;
  reloj.innerHTML=Strmin+" : "+Strsec+" : "+Strmil; 
  setTimeout("timer()", 1000);	// waits one second and repeats
 }
}
function startover(){		// This function resets
keepgoin=false;			// all the variables
currentsec=5;
currentmin=59;
currentmil=99;
Strsec="59";
Strmin="59";
Strmil="99";
}

var cargador = {
	loadState: false,
	objsToLoad: 13,
	objsLoaded: 0,
	sceneIsReady: false,
	objReady: function(){
		this.objsToLoad--;
		this.objsLoaded++;
		var total = this.objsToLoad+this.objsLoaded;
		var porcentaje = (this.objsLoaded/total)*100;
		$("#barraDeCarga div#porcentaje").html(porcentaje+"%");
		$("#barraDeCarga div#carga").css("width",porcentaje+"%");
		if(this.objsToLoad == 0){
			this.loadState = true;
			$('#loader').slideUp(2000);
			$('#game').slideUp(2000);
		}
	}
};
var ArregloCarro = {};
var VariablesCarro = [];

var ArregloEscenario = {};
var ArregloPerro = {};
var APerro = [];
var collidableMeshList = [];
var ArrayZombies = [];
var Audios=[];
													
													//VARIABLES

													var manejabilidad ;
													var movSpeed;					
													var frenos;
													var nitro;
													var C1;
													var C2;
													var C3;
													var C4;
													var C5;

													var maxSpeed;
													var minSpeed = maxSpeed*-1;
													var friccion = 1;
													var Speed = 0;
													var cambio_actual = 7;
													var cambio_anterior = 7;
													var bloqueo = false;
													var reversa = false;
													var rotateAngle = 0;
													var bloqueo = false;
													var BarraNitro = 100;

													var puntaje = 0;
var max =  5000;
var min = -5000;

var RandomX;
var RandomZ;

function webGLStart(){

	clock = new THREE.Clock;

	iniciarEscena();

	ultiTiempo = Date.now();
	document.onkeydown = teclaPulsada;
	document.onkeyup = teclaSoltada;
	animarEscena();
}

function iniciarEscena(){
	
	//RENDERER
	renderer = new THREE.WebGLRenderer( {antialias: true} );
	renderer.setSize( appW , appH);
	renderer.setClearColorHex( 0xAAAAAA, 1.0);
	renderer.shadowMapEnabled = true;

	document.body.appendChild(renderer.domElement);

	//Scene
	scene = new THREE.Scene();





var mensaje = "Tiempo de elegir el coche... \n Digita '1' para jugar con el Porsche. \n Digita '2' para jugar con el policia. \n Digita '3' para jugar con el batimobil."

varCargar = prompt(mensaje,'1'); 



if (varCargar == 1) {
	cargarPorsche();
}else
if (varCargar == 2) {
	cargarCop();
}else
if (varCargar == 3) {
	cargarBat();
}else{
	cargarPorsche();
}

cargarEscenario();




	//AUDIO
	Encendido 	= new Sound(['sonidos/motorAnda.mp3'],1600,1);
	
	AudPrimera 	= new Sound(['sonidos/1000RP.mp3'],1600,1);
	AudSegunda 	= new Sound(['sonidos/2000RP.mp3'],1600,1);
	AudTercera 	= new Sound(['sonidos/3000RP.mp3'],1600,1);
	AudCuarta 	= new Sound(['sonidos/4000RP.mp3'],1600,1);
	AudQuinta 	= new Sound(['sonidos/5000RP.mp3'],1600,1);
	Zombie = new Sound(['sonidos/Zombie.mp3'],1600,1);

	Audios.push(Encendido);
	Audios.push(AudPrimera);
	Audios.push(AudSegunda);
	Audios.push(AudTercera);
	Audios.push(AudCuarta);
	Audios.push(AudQuinta);
	Audios.push(Zombie);

	Audios[0].play();



	//CAMARAS
	var view_angle = 45, aspect_ratio = appW/appH, near = 1, far = 20000;
	camera = new THREE.PerspectiveCamera( view_angle, aspect_ratio, near, far);
	camera.position.set( 0,200,650 );
	
	//CONTROL DE LA CAMARA
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement);

	//ESTADÍSTICAS
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild( stats.domElement );

	//Luces
	luzSpotLight = new THREE.SpotLight(0xffffff);
	luzSpotLight.position.set(0,150,0);
	bombilla = new THREE.Mesh(new THREE.SphereGeometry(1,1,1),new THREE.MeshBasicMaterial({color:0xffff00, wireframe:false}));
	
	pivot = new THREE.Object3D();
	pivot.name = "Prosche 911 GT";


	//TERRENO COn CARGA  DE PNG
	var img = new Image();
	img.onload = function(){
		var data = getHeightData(img);
		geometry = new THREE.PlaneGeometry(10000,10000,199,199);
		texture = THREE.ImageUtils.loadTexture('./Circuito BN/DesiertC.jpg');
		material = new THREE.MeshLambertMaterial( {map: texture, ambient: 0x000000} );	
		for (var i = 0; i < geometry.vertices.length; i++) {
			geometry.vertices[i].z = data[i];
		};
		pngTerrain = new THREE.Mesh( geometry, material );
		pngTerrain.rotation.x = -Math.PI/2;
		pngTerrain.scale.set(1,1,5);
		pngTerrain.position.set(0,-6.5,0);
		scene.add(pngTerrain);
	};
	img.src = './terrenos/DesierC.png';


	//Sybox
	var imagePrefix = "./Skybox/Cielo-";
	var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
	var imageSuffix = ".jpg";
	var skyGeometry = new THREE.CubeGeometry( 10000, 8000, 10000 );	
	var materialArray = [];
	for (var i = 0; i < 6; i++)
		materialArray.push( new THREE.MeshBasicMaterial({
			map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
			side: THREE.BackSide
		}));
	var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
	skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
	scene.add(skyBox);


collisionBox1 = new THREE.Mesh(new THREE.CylinderGeometry( 30, 30, 3000, 32 ),
				                   new THREE.MeshBasicMaterial({color:0x00ff00, transparent: true, opacity: 0.1})
				                  );
	collisionBox1.position.set(-15,1400,-400);
	collisionBox1.rotation.y = Math.PI/2;
	ArrayZombies.push(collisionBox1);
	scene.add(collisionBox1);



	var PerroLoader = new THREE.JSONLoader();
		var PerroFunction = function( geometry )
		{	
			var stopMaterial = new THREE.MeshPhongMaterial({map: new THREE.ImageUtils.loadTexture('./Meshes/perro/dog_crap.jpg')});
		    var Perro = new THREE.Mesh( geometry, stopMaterial );
		    Perro.position.set( collisionBox1.position.x, 0, collisionBox1.position.z );
		    //Perro.rotation.y = -Math.PI/2;
		    Perro.scale.set( 5, 5, 5 );
		    Perro.castShadow = true;
		    Perro.receiveShadow = false;
		    Perro.overdraw = true;
		    ArregloPerro.Perro = Perro; 
		    scene.add(ArregloPerro["Perro"]);
		    APerro.push(ArregloPerro["Perro"]);
		    cargador.objReady();
		};
		PerroLoader.load( "./Meshes/perro/perro.js", PerroFunction );



	

	




}

function llenarEscena() {
													manejabilidad = VariablesCarro[0];
													movSpeed = VariablesCarro[1];								
													frenos = VariablesCarro[2];
													nitro = VariablesCarro[3];

													C1 = VariablesCarro[4];
													C2 = VariablesCarro[5];
													C3 = VariablesCarro[6];
													C4 = VariablesCarro[7];
													C5 = VariablesCarro[8];

	
	console.log(movSpeed);



	tacometro.style.visibility="visible";
	aguja.style.visibility="visible";
	
	cronometro.style.visibility="visible";
	zombie.style.visibility="visible";
	score.style.visibility="visible";
	barra.style.visibility="visible";
	Vnitro.style.visibility="visible";
	

	camera.lookAt(ArregloCarro["Carroceria"].position);
	controlCamara.center = ArregloCarro["Carroceria"].position;

	scene.fog = new THREE.Fog( 0x808080, 1000, 7500 );

	scene.add( camera );
	camera.lookAt(scene.position);

	// Luces
	luzSpotLight = new THREE.SpotLight(0xFFBF75);
	luzSpotLight.position.set(0,15000,0);
	luzSpotLight.shadowCameraVisible = false; // Guias de luz

	luzSpotLight.shadowDarkness = 1; // Opacidad de sombras
	luzSpotLight.intensity = 0.4; // Intensidad de Luz
	luzSpotLight.castShadow = true; // Castea Sombras?
	luzSpotLight.target = ArregloCarro["Carroceria"];
	//Shadow map texture width in pixels.
	luzSpotLight.shadowMapWidth = 1500;
	luzSpotLight.shadowMapHeight = 1500;
	luzSpotLight.shadowCameraNear = 7000;
	luzSpotLight.shadowCameraFar = 15500;
	luzSpotLight.shadowCameraFov = 40;


	scene.add(luzSpotLight);

	//Elementos	
	pivot.add( ArregloCarro["Carroceria"] ); 	//0
	pivot.add( ArregloCarro["Glasses"] );		//1
	pivot.add( ArregloCarro["R_wheel"] );		//2
	pivot.add( ArregloCarro["L_wheel"] );		//3
	pivot.add( ArregloCarro["Rear_wheels"] );	//4
	pivot.add( camera );						//5
	pivot.add(ArregloCarro["BB"]);						//6

	scene.add( pivot );

	
}


function animarEscena(){
	requestAnimationFrame( animarEscena );

	if(!cargador.loadState){
		console.log("Obj Loaded : "+cargador.objsLoaded+" / "+(cargador.objsToLoad+cargador.objsLoaded));
	}else{
		if(!cargador.sceneIsReady){
			llenarEscena();
			console.log("Obj Loaded: "+cargador.objsLoaded+" from "+(cargador.objsToLoad+cargador.objsLoaded));
			console.log("scene Ready");
			cargador.sceneIsReady = true;

		}
		renderEscena();
		actualizarEscena();
	}
}

function renderEscena(){
	renderer.render( scene, camera );
}


/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////----actualizarEscena----////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////



function actualizarEscena(){
	if (Speed >5) {keepgoin = true};
	if (currentmin < 0) {
		keepgoin = false;
		cambio_actual = 7;
		Speed = 0;
		
		alert("LOSER!!!!!!!");
		startover();
		document.href="index.html";
	};

	if (puntaje >=ganar) {
		puntaje = 0;
		cambio_actual = 7;
		alert("WINNER!!!!!!!");
		startover();
		document.href="index.html";
	};
	timer();

	RandomX = Math.floor(Math.random()*(max-min+1)+min);
	RandomZ = Math.floor(Math.random()*(max-min+1)+min);

	score.innerHTML=puntaje;

	ultiTiempo = Date.now();
	controlCamara.update();

	var time = Date.now() *0.0005;
	var delta = clock.getDelta();
	

	//Speed = aceleracion * delta; // 200 pixels per second
 	rotateAngle = ((Math.PI / 4 * manejabilidad)*Speed/50);   // pi/4 radians (45 degrees) per second
 	//console.log(rotateAngle);
 	//console.log(Speed);
 	//console.log(movSpeed);

	if (Speed > 0 ) {Speed -= friccion};
	if (Speed < 0 ) {Speed += friccion};
	if (Speed > 0 && (Speed < 1 || Speed <-2)) {Speed = 0};

	if (Speed >= maxSpeed) {Speed = maxSpeed};


	pivot.translateZ( -Speed );
	luzSpotLight.position.set(pivot.position.x,15000,pivot.position.z);
	skyBox.position.set(pivot.position.x,100,pivot.position.z);
	
	//console.log(maxSpeed);
	//console.log(Speed);
	//moveObject(pivot , new THREE.Vector3(0,0,1) , -Speed);
	//ArregloCarro.Rear_wheels.rotation.x -= Speed/2;

	///LAS LANTAS REGRESAN A SU ROTACION INICIAL
	if (ArregloCarro["R_wheel"].rotation.y > 0) {ArregloCarro["R_wheel"].rotation.y -= Math.abs(rotateAngle)*4};
	if (ArregloCarro["R_wheel"].rotation.y < 0) {ArregloCarro["R_wheel"].rotation.y += Math.abs(rotateAngle)*4};
	if (ArregloCarro["L_wheel"].rotation.y > 0) {ArregloCarro["L_wheel"].rotation.y -= Math.abs(rotateAngle)*4};
	if (ArregloCarro["L_wheel"].rotation.y < 0) {ArregloCarro["L_wheel"].rotation.y += Math.abs(rotateAngle)*4};
	

	//MOVIMIENTO DE LA LLANATAS TRASERAS
	if (Speed > 0) {
		if (!bloqueo) {
			ArregloCarro["Rear_wheels"].rotation.x += (Speed)/4;
		};
	};
	if (Speed < 0) {
		if (!bloqueo) {
		ArregloCarro["Rear_wheels"].rotation.x -= (Speed)/8;
		};	
	};

	aguja.style.transform="rotateZ("+Math.abs(Speed)+"deg)";
	Vnitro.style.width = (BarraNitro/3)+"%"

	switch(cambio_actual){
		case 1: //0 a 15
			maxSpeed = C1;
			bloqueo = false;
			tacometro.src="img/tacometro/1.png";
			if (Speed > 10) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[cambio_actual].play();
			};
		break;
		case 2://15 a 30
			maxSpeed = C2;
			bloqueo = false;
			tacometro.src="img/tacometro/2.png";
			if (Speed > 0) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[cambio_actual].play();
			};
		break;
		case 3://30 a 50
			maxSpeed = C3;
			bloqueo = false;
			tacometro.src="img/tacometro/3.png";
			if (Speed > 0) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[cambio_actual].play();
			};
		break;
		case 4://50 a 70
			maxSpeed = C4;
			bloqueo = false;
			tacometro.src="img/tacometro/4.png";
			if (Speed > 0) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[cambio_actual].play();
			};
		break;
		case 5://70 a 150
			maxSpeed = C5;
			bloqueo = false;
			tacometro.src="img/tacometro/5.png";
			if (Speed > 0) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[cambio_actual].play();
			};
		break;
		case 6://reversa
			maxspeed = C5;
			bloqueo = true;
			tacometro.src="img/tacometro/R.png";
			if (Speed < 0) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[3].play();
			};
		break;
		case 7://neutro
			bloqueo = true;
			tacometro.src="img/tacometro/N.png";
			if (Speed > 0) {
				for (var i = 0; i < Audios.length; i++) {
					if (i != 6) {
						Audios[i].pause();		
					};
				};
				Audios[0].play();
			};
		break;

	}
	//console.log(Speed);
	if (puntaje >= 2) {
		zombie.src="img/Zombies/lvl2.png"; 	
	};

	if (puntaje >= 3) {
		zombie.src="img/Zombies/lvl3.png"; 	

	};
	if (puntaje >= 4) {
		zombie.src="img/Zombies/lvl4.png"; 	
	};
	if (puntaje >= 5) {
		zombie.src="img/Zombies/lvl5.png"; 	
	};

	////////////////////////CONDICONALES//////////////////////////////
	if(TECLA.ESPACIO){
		Speed *= frenos;
		//console.log(Speed);
	}
	if(TECLA.ARRIBA){
		if (bloqueo == false) {
			if (Speed < 2 && cambio_actual == 1) {
				Speed += (movSpeed);
			}else if(Speed < 2 && cambio_actual > 2){
				Speed = 0;
			}else if(Math.abs(cambio_anterior - cambio_actual) > 1){
				Speed = 0;
			}else{
				Speed += (movSpeed);
			}
		};
	if(TECLA.ARRIBA && TECLA.NITRO){
		if (BarraNitro > 0) {
			switch(cambio_actual){
				case 1: //0 a 15
					maxSpeed = C1*2;


				break;
				case 2://15 a 30
					maxSpeed = C2*2;

				break;
				case 3://30 a 50
					maxSpeed = C3*2;

				break;
				case 4://50 a 70
					maxSpeed = C4*2;

				break;
				case 5://70 a 150
					maxSpeed = C5*2;

				break;
				case 6://reversa
					maxspeed = C5;
				break;
				case 7://neutro
					bloqueo = true;

				break;
			}
			Speed *= nitro;
			BarraNitro--;
		};
		
		if (BarraNitro < 0) {BarraNitro = 0};
		console.log(BarraNitro);
	}
			
	}
	if(TECLA.ABAJO){
		if (Speed < 0 && cambio_actual != 6) {
			Speed = 0;
		}else{
			Speed -= (movSpeed)/1.4;
		}
	}
	if(TECLA.IZQUIERDA){
		rotateAroundWorldAxis(pivot , new THREE.Vector3(0,1,0) , Math.abs(rotateAngle));
		//rotateAroundWorldAxis(boundingBox , new THREE.Vector3(0,1,0) , manejabilidad);
		if (ArregloCarro["L_wheel"].rotation.y <= 6*(Math.PI/32)) {
			ArregloCarro["L_wheel"].rotation.y += Math.abs(rotateAngle)*8;	
		};
		if (ArregloCarro["R_wheel"].rotation.y <= 6*(Math.PI/32)) {
			ArregloCarro["R_wheel"].rotation.y += Math.abs(rotateAngle)*8;
		};
		//pivot.rotateOnAxis( new THREE.Vector3(0,1,0), Math.abs(rotateAngle));
	}
	if(TECLA.DERECHA){
		rotateAroundWorldAxis(pivot , new THREE.Vector3(0,1,0) , -Math.abs(rotateAngle));
		//rotateAroundWorldAxis(boundingBox , new THREE.Vector3(0,1,0) , -manejabilidad);
		if (ArregloCarro["L_wheel"].rotation.y >= 6*(-Math.PI/32)) {
				ArregloCarro["L_wheel"].rotation.y -= Math.abs(rotateAngle)*8;	
			};
			if (ArregloCarro["R_wheel"].rotation.y >= 6*(-Math.PI/32)) {
				ArregloCarro["R_wheel"].rotation.y -= Math.abs(rotateAngle)*8;
			};
			pivot.rotateOnAxis( new THREE.Vector3(0,1,0), -Math.abs(rotateAngle));
	}


///////////////////////////////FIN CONDICIONALES//////////////////////////////////////////

//////////////////////////////CAJA DE CAMBIOS///////////////////////////////
	if(TECLA.PRIMERA){//0 a 15
		cambio_anterior = cambio_actual;
		cambio_actual = 1;
		console.log(cambio_actual);
	}
	if(TECLA.SEGUNDA){//15 a 30
		cambio_anterior = cambio_actual;
		if (Speed >= 10) {
			cambio_actual = 2;
		}else{
		}
		console.log(cambio_actual);
	}
	if(TECLA.TERCERA){//30 a 50
		cambio_anterior = cambio_actual;
		if (Speed >= 25) {
			cambio_actual = 3;
		}else{
		}
		console.log(cambio_actual);
	}
	if(TECLA.CUARTA){//50 a 70
		cambio_anterior = cambio_actual;
		if (Speed >= 45) {
			cambio_actual = 4;
		}else{
		}
		console.log(cambio_actual);
	}
	if(TECLA.QUINTA){//70 a maxspeed
		cambio_anterior = cambio_actual;
		if (Speed >= 65) {
			cambio_actual = 5;
		}else{
		}
		console.log(cambio_actual);
	}
	if(TECLA.REVERSA){// -0 a -maxspeed
		cambio_anterior = cambio_actual;
		cambio_actual = 6;
		console.log(cambio_actual);
	}
	if(TECLA.NEUTRO){
		cambio_anterior = cambio_actual;
		cambio_actual = 7;
		console.log(cambio_actual);
	}

//////////////////////////////FIN CAJA DE CAMBIOS///////////////////////////////////////


	/*
	/////////////////////////COLISIONES////////////////////////////////
	var originPoint = pivot.children[6].position.clone();
	//var originPoint = camaro.position.clone();

	for (var vertexIndex = 0; vertexIndex < pivot.children[6].geometry.vertices.length; vertexIndex++)
	{		
		var localVertex = pivot.children[6].geometry.vertices[vertexIndex].clone();
		var globalVertex = localVertex.applyMatrix4( pivot.children[6].matrix );
		var directionVector = globalVertex.sub( pivot.children[6].position );
		
		var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );
		var collisionResults = ray.intersectObjects( collidableMeshList );
		if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() ) {
			console.log('DAMN!');
			moveObject(pivot.children[6] , new THREE.Vector3(0,0,1) , -movSpeed/2);
			moveObject(pivot , new THREE.Vector3(0,0,1) , -movSpeed/2);
		}

	}
	*/
	var pivotBBox = pivot.children[6];
	var puntoDeOrigen = pivot.children[6].position.clone();
	var puntoDeOrigenpivot = pivot.position.clone();
/*	console.log("puntoDeOrigen: ");
	console.log(puntoDeOrigen);
	console.log("PuntoDeOrigenpivot: ");
	console.log(puntoDeOrigenpivot);*/
	for (var i = 0; i < pivotBBox.geometry.vertices.length; i++) {
		var localVertex = pivotBBox.geometry.vertices[i].clone();
		var globalVertex = localVertex.applyMatrix4( pivotBBox.matrix );
		var directionVector = globalVertex.sub( pivotBBox.position );

		var ray = new THREE.Raycaster( puntoDeOrigenpivot, directionVector.clone().normalize());
		var collisionResults = ray.intersectObjects( collidableMeshList );
		var zombieAtropelaldo = ray.intersectObjects( ArrayZombies );

		if(collisionResults.length> 0 && collisionResults[0].distance-35 < directionVector.length()){
			console.log('BAM!!!');
			return moveObject(pivot , new THREE.Vector3(0,0,1) , +movSpeed*10);
		}

		
	};
	if(zombieAtropelaldo.length> 0 && zombieAtropelaldo[0].distance-40 < directionVector.length()){
			console.log('TOMA ESO!!!');
			document.getElementById("sangre").style.visibility = "visible";
			$( "#sangre" ).fadeIn( "fast" );
  			$( "#sangre" ).fadeOut( 3000);
			
			scene.remove(ArrayZombies[0]);
			ArrayZombies.pop();

			//scene.remove(APerro[0]);
		    //APerro.pop();

			puntaje++;
			console.log(puntaje);

			currentsec+=30;

			if (BarraNitro <= 90) {
				BarraNitro += 10;	
			};
			
			Audios[6].playOnce();

			collisionBox = new THREE.Mesh(new THREE.CylinderGeometry( 10, 80, 3000, 32 ),
				                   new THREE.MeshBasicMaterial({color:0x00ff00, transparent: true, opacity: 0.1})
				                  );
			collisionBox.position.set(RandomX,1400,RandomZ);
			collisionBox.rotation.y = Math.PI/2;

			ArrayZombies.push(collisionBox);
			scene.add(collisionBox);

		    APerro[0].position.set( collisionBox.position.x, 0, collisionBox.position.z );

		}
	stats.update();
}

			
